(function() {
    var app = angular.module('kinsella', ['onsen.directives', 'ngTouch','autocomplete']);
   
    app.factory('canvas',function(){
      return {
        setSpinner: function(spinnerId){
          var canvas = document.getElementById(spinnerId);
          var context = canvas.getContext('2d');  
          var start = new Date();
          var lines = 16,  
          cW = context.canvas.width,
          cH = context.canvas.height;
          var setarray = [canvas,context,start,lines,cW,cH];
          return setarray;
        },
        getSpinner: function(canvas,context,start,lines,cW,cH){
          window.setInterval(function(){
             var rotation = parseInt(((new Date() - start) / 1000) * lines) / lines;
              context.save();
              context.clearRect(0, 0, cW, cH);
              context.translate(cW / 2, cH / 2);
              context.rotate(Math.PI * 2 * rotation);
              for (var i = 0; i < lines; i++) {

                  context.beginPath();
                  context.rotate(Math.PI * 2 / lines);
                  context.moveTo(cW / 10, 0);
                  context.lineTo(cW / 4, 0);
                  context.lineWidth = cW / 30;
                  context.strokeStyle = "rgba(0, 0, 0," + i / lines + ")";
                  context.stroke();
              }
              context.restore();
          },1000/30);

        }
       }
    });
  

    app.controller('QuestionCtrl', function($scope,canvas) {
    $scope.isDisabled = false;

      // Would write the value of the QueryString-variable called name to the console  
      $scope.wibbly = function(userid){
          var nexttext = document.getElementById('next-text');
              nexttext.className='no-display'; 
          var nextimage = document.getElementById('spinner');
              nextimage.className=''; 

          var question = document.getElementById("question-number").innerHTML; 
          question = parseInt(question)+1;
          if($scope.isDisabled==false){
                  $scope.ons.navigator.resetToPage('https://smackagency.com/kinsella-consequences?question=' + question + '&userid=' + userid, {animation: "fade"});
                  $scope.isDisabled = true;
          }

      };

      $scope.wobbly = function(){
          $scope.start=canvas.setSpinner('spinner2');
          $scope.spin = canvas.getSpinner($scope.start[0],$scope.start[1],$scope.start[2],$scope.start[3],$scope.start[4],$scope.start[5]);
          //canvas.getSpinner($scope.start);
          //window.setInterval(canvas.getSpinner, 1000 / 30);
          var nexttext = document.getElementById('next-text2');
              nexttext.className='no-display'; 
          var nextimage = document.getElementById('spinner2');
              nextimage.className=''; 

          if($scope.isDisabled==false){
                   $scope.ons.navigator.resetToPage('thanks', {animation: "fade"});
                  $scope.isDisabled = true;
          }

      };

    });

 app.controller('StartCtrl', function($scope,canvas) {
    $scope.isDisabled = false;

    

      document.addEventListener('deviceready', function() {
        document.addEventListener('backbutton', function() { navigator.app.exitApp();},false); // eval to stop closure });
      },false);
      // Would write the value of the QueryString-variable called name to the console  
      $scope.wibbly = function(){
          var nexttext = document.getElementById('next-text');
              nexttext.className='no-display'; 
          var nextimage = document.getElementById('spind');
              nextimage.className=''; 

          if($scope.isDisabled==false){
                  $scope.ons.navigator.resetToPage('play.html' , {animation: "fade"});
                  $scope.isDisabled = true;
          }

      };

    });

 app.controller('PlayCtrl', function($scope,canvas) {
    $scope.isDisabled = false;

      // Would write the value of the QueryString-variable called name to the console  
      $scope.wibbly = function(){
          var nexttext = document.getElementById('next-text');
              nexttext.className='no-display'; 
          var nextimage = document.getElementById('spinp');
              nextimage.className=''; 

          if($scope.isDisabled==false){
                  $scope.ons.navigator.resetToPage('https://smackagency.com/kinsella-consequences?question=1' , {animation: "fade"});
                  $scope.isDisabled = true;
          }

      };

    });

 app.controller('ThanksCtrl', function($scope,canvas) {
    $scope.isDisabled = false;

      // Would write the value of the QueryString-variable called name to the console  
      $scope.wibbly = function(){
          var nexttext = document.getElementById('next-text');
              nexttext.className='no-display'; 
          var nextimage = document.getElementById('spint');
              nextimage.className=''; 

          if($scope.isDisabled==false){
                  $scope.ons.navigator.resetToPage('start.html' , {animation: "fade"});
                  $scope.isDisabled = true;
          }

      };

    });


})();
